# Daniel Behrens
# Computational HW 4 - Python Code

#####################################################################################
#Taken from 'PYTHON EXAMPLE 2: planning2.py' in lecture slides:
#####################################################################################

#! /usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)


robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()    
move_group = moveit_commander.MoveGroupCommander("manipulator")

#####################################################################################
#Taken from 'PYTHON EXAMPLE 3: get_data.py' in lecture slides:
#####################################################################################

print("Reference frame: %s" % move_group.get_planning_frame())

print("End effector: %s" % move_group.get_end_effector_link())

print("Robot Groups:")
print(robot.get_group_names())

print("Current Joint Values: ")
print(move_group.get_current_joint_values())

print("Current Pose: ")
print(move_group.get_current_pose())

print("Robot State:")
print(robot.get_current_state())


######################################################################################
#Based on 'PYTHON EXAMPLE 4: execute_TRAJECTORY.py' in lecture slides:
######################################################################################

pose_goal = geometry_msgs.msg.Pose()
pose_goal.orientation.w = 1.0
pose_goal.position.x = 0.4
pose_goal.position.y = 0.1
pose_goal.position.z = 0.4

move_group.set_pose_target(pose_goal)

plan = move_group.go(wait=True)

# Calling `stop()` ensures that there is no residual movement
move_group.stop()

# It is always good to clear your targets after planning with poses.
# Note: there is no equivalent function for clear_joint_value_targets()
move_group.clear_pose_targets()

#####################################################################################
# Setting waypoints: the robot should configure itself to the pose specified above,
# then move according to each waypoint in order to spell out my first initial, 'D'
#####################################################################################

waypoints = []
scale = 1.0
wpose = move_group.get_current_pose().pose

wpose.position.z += scale * 0.1  # Moves up vertically (+z) to make the stem of the 'D'
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.05  # Second move goes diagonally down and right to form
wpose.position.z -= scale * 0.05  # the top half of the curve of the 'D'
waypoints.append(copy.deepcopy(wpose))

wpose.position.x -= scale * 0.05  # Third move goes diagonally down and left to make the
wpose.position.z -= scale * 0.05  # bottom half of the curve and complete the 'D'
waypoints.append(copy.deepcopy(wpose))

# The final shape traced is a small triangle that looks somewhat like a capital 'D'
# It also sort of looks like a 'play' button, but in the context of letters it's a 'D'


(plan, fraction) = move_group.compute_cartesian_path(
            waypoints, 0.01, 0.0  # waypoints to follow  # eef_step
            )  # jump_threshold

move_group.execute(plan, wait=True)

moveit_commander.roscpp_shutdown()

