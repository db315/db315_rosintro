# Daniel Behrens
# Midterm Project
# October 13, 2021

###############################################################################
#Taken from 'PYTHON EXAMPLE 2: planning2.py' in lecture slides:
###############################################################################

#! /usr/bin/env python
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg


moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)


robot = moveit_commander.RobotCommander()
scene = moveit_commander.PlanningSceneInterface()    
group = moveit_commander.MoveGroupCommander("manipulator")
display_trajectory_publisher = rospy.Publisher('/move_group/display_planned_path', moveit_msgs.msg.DisplayTrajectory)


###############################################################################
# STARTING TO DRAW: FIRST INITIAL ('D')
###############################################################################

###############################################################################
# SETTING JOINT GOALS:
# - This section tells the robot to configure each joint to a specified angle,
#   resulting in the pose that will begin the first letter
# - Code modified from the "Planning to a Joint Goal" section of the MoveIt
#   tutorial
###############################################################################

# We get the joint values from the group and change some of the values:
pi = 3.1415926536
joint_goal = group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -pi/6 # -30 deg
joint_goal[2] = pi/6 # 30 deg
joint_goal[3] = 0
joint_goal[4] = 0
joint_goal[5] = 0

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
group.stop()

###############################################################################
# SETTING WAYPOINTS
# - After the robot reaches the starting pose, it will execute the following
#   movements according to the waypoints specified
# - These movements draw my first initial (D) in space
# - Code modified from the "Cartesian Paths" section of the MoveIt tutorial
###############################################################################

waypoints = []
scale = 1.0
wpose = group.get_current_pose().pose

# First, make the robot move up vertically (+z) to make the stem of the 'D'
wpose.position.z += scale * 0.16
waypoints.append(copy.deepcopy(wpose))

# Next, move the end-effector diagonally down and right(-x), forming the top
# half of the curve of the 'D'
wpose.position.x -= scale * 0.08
wpose.position.z -= scale * 0.08
waypoints.append(copy.deepcopy(wpose))

# The third movement goes diagonally down and left to make the bottom half
# of the curve and finish the 'D'
wpose.position.x += scale * 0.08
wpose.position.z -= scale * 0.08
waypoints.append(copy.deepcopy(wpose))

# The completed shape is meant to be a triangle with a flat edge on the left,
# somewhat like a 'play' button on a remote, which resembles a capital 'D'

(plan, fraction) = group.compute_cartesian_path(waypoints, 0.01, 0.0)
group.execute(plan, wait=True)

###############################################################################
# END OF FIRST INITIAL
###############################################################################


###############################################################################
# SECOND INITIAL:
# - Same process as first initial: specify starting point via joint angles,
#   then use waypoints to draw initial in space
# - This will draw 'J' slightly above / to the right of the first initial 'D'
###############################################################################

# SETTING JOINT ANGLES:

# We get the joint values from the group and change some of the values:
pi = 3.1415926536
joint_goal = group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -pi/2 # -90 deg
joint_goal[2] = pi/2 # 90 deg
joint_goal[3] = 0
joint_goal[4] = 0
joint_goal[5] = 0

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
group.stop()


# SETTING WAYPOINTS:

waypoints = []
scale = 1.0
wpose = group.get_current_pose().pose

# First, make the robot move right to make the bottom curve of 'J'
wpose.position.x -= scale * 0.08
waypoints.append(copy.deepcopy(wpose))

# Next, move the end-effector vertically to form the straight, right edge
wpose.position.z += scale * 0.16
waypoints.append(copy.deepcopy(wpose))

# Complete shape is just a right angle, like a backwards 'L', meant to
# roughly look like a 'J'

(plan, fraction) = group.compute_cartesian_path(waypoints, 0.01, 0.0)
group.execute(plan, wait=True)

###############################################################################
# END OF SECOND INITIAL
###############################################################################

###############################################################################
# THIRD INITIAL:
# - Same process as first and second initial: specify starting point via joint 
#   angles, then use waypoints to draw initial in space
# - This will draw 'B' slightly below / to the right of the second initial 'J'
###############################################################################

# SETTING JOINT ANGLES:

# We get the joint values from the group and change some of the values:
pi = 3.1415926536
joint_goal = group.get_current_joint_values()
joint_goal[0] = 0
joint_goal[1] = -pi*2/3 # -120 deg
joint_goal[2] = pi*2/3 # 120 deg
joint_goal[3] = 0
joint_goal[4] = 0
joint_goal[5] = 0

# The go command can be called with joint values, poses, or without any
# parameters if you have already set the pose or joint target for the group
group.go(joint_goal, wait=True)

# Calling ``stop()`` ensures that there is no residual movement
group.stop()


# SETTING WAYPOINTS:

waypoints = []
scale = 1.0
wpose = group.get_current_pose().pose

# First, make the robot move up vertically (+z) to make the stem of the 'B'
wpose.position.z += scale * 0.16
waypoints.append(copy.deepcopy(wpose))

# Next, move the end-effector diagonally down and right(-x), forming the top
# half of the upper bump of the 'B'
wpose.position.x -= scale * 0.08
wpose.position.z -= scale * 0.04
waypoints.append(copy.deepcopy(wpose))

# The third movement goes diagonally down and left to make the bottom half
# of the upper bump of the 'B'
wpose.position.x += scale * 0.08
wpose.position.z -= scale * 0.04
waypoints.append(copy.deepcopy(wpose))

# The lower bump of the 'B' is made the same way as the first, so the second
# and third waypoint instructions are simply repeated
wpose.position.x -= scale * 0.08
wpose.position.z -= scale * 0.04
waypoints.append(copy.deepcopy(wpose))

wpose.position.x += scale * 0.08
wpose.position.z -= scale * 0.04
waypoints.append(copy.deepcopy(wpose))

# The final traced shape is two triangles on top of each other, sharing a
# vertical left side and pointing to the right, making a pointy 'B' shape

(plan, fraction) = group.compute_cartesian_path(waypoints, 0.01, 0.0)
group.execute(plan, wait=True)

###############################################################################
# END OF THIRD INITIAL
###############################################################################

# closing instruction to tell moveit to stop:
moveit_commander.roscpp_shutdown()

